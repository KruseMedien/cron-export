<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
define('KM12_CRONEXPORT_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);
$dispatcher = $container['sly-dispatcher'];

// Container & Services
$container['sly-classloader']->add('', __DIR__.'/lib');
$container['sly-i18n']->appendFile(__DIR__.'/lang');


$container['sly-cronexport-service'] = $container->share(function($container) {
	$config   = $container['sly-service-addon']->getProperty('krusemedien/cron-export', '', array());

	return new sly\CronExport\Service($config);
});

$container['sly-cronexport-listeners'] = $container->share(function($container) {
	return new sly\CronExport\Listeners();
});

if (!sly_Core::isBackend()) {
	$dispatcher->register('SLY_FRONTEND_ROUTER', 'sly_Controller_Frontend_Cronexport::addRoutes');
}else {
	$dispatcher->addListener('SLY_BACKEND_NAVIGATION_INIT',     array('%sly-cronexport-listeners%', 'backendNavigation'));
}