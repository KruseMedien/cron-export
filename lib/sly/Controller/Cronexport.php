<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
use sly\Assets\Util;
use sly\CronExport\TimeParser;
class sly_Controller_Cronexport extends sly_Controller_Backend implements sly_Controller_Interface {
	public function indexAction() {
		$name      = 'krusemedien/cron-export';
		$container = $this->getContainer();
		$layout    = $container['sly-layout'];
		$service   = $container['sly-service-addon'];
		$config    = $container['sly-cronexport-service']->getConfig(null);
		$version   = $service->getPackageService()->getVersion($name);

		$layout->addJavascriptFile(Util::addOnUri($name, 'js/cron-export.js'));
		$layout->addCSSFile(Util::addOnUri($name, 'css/ce-backend.less'));
		$layout->pageHeader(t('ce_title'));

		print sly_Helper_Message::renderFlashMessage($container['sly-flash-message']);

		$user = $this->getCurrentUser();
		$canAccessUsers = $user->isAdmin() || $user->hasPermission('pages', 'user');

		$this->render('cron.phtml', array(
			'config'  => $config
		), false);
	}

	public function updateAction() {
		sly_Util_Csrf::checkToken();

		$request  = $this->getRequest();
		$salt     = $request->post('salt', 'string');
		$comment     = $request->post('comment', 'string');
		$users     = $request->post('users', 'string');
		$deletebackups     = $request->post('deletebackups', 'string');
		$delay     = TimeParser::decodeString($request->post('delay', 'string'));
		$count     = $request->post('count', 'string');

		$container   = $this->getContainer();
		$service     = $container['sly-service-addon'];
		$name        = 'krusemedien/cron-export';

		$service->setProperty($name, 'salt',    $salt);
		$service->setProperty($name, 'comment',    $comment);
		$service->setProperty($name, 'users',    $users);
		$service->setProperty($name, 'deletebackups',    $deletebackups);
		$service->setProperty($name, 'delay',    $delay);
		$service->setProperty($name, 'backupcount',    $count);

		$container['sly-flash-message']->appendInfo(t('ce_config_saved'));

		return $this->redirectResponse();
	}

	public function checkPermission($action) {
		$user = $this->getContainer()->get('sly-service-user')->getCurrentUser();

		return $user && ($user->isAdmin() || $user->hasRight('pages', 'cron-export'));
	}

	protected function getViewFolder() {
		return KM12_CRONEXPORT_PATH.'/templates/';
	}
}
