<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
use sly\CronExport\Util;

class sly_Controller_Frontend_Cronexport extends sly_Controller_Frontend_Base {

	public function __construct() {
		sly_Core::setCurrentClang(sly_Core::getDefaultClangId()) ;

		$timezone = sly_Core::config()->get('SETUP') ? @date_default_timezone_get() : sly_Core::getTimezone();
		// fix badly configured servers where the get function doesn't even return a guessed default timezone
		if (empty($timezone)) {
			$timezone = sly_Core::getTimezone();
		}
		// set the determined timezone
		date_default_timezone_set($timezone);
		
		//sly_Service_Factory::getArticleService();

	}

	public function indexAction() {
		$request = sly_Core::getRequest();
		$container = sly_Core::getContainer();
		$service     = $container['sly-service-addon'];
		$name        = 'krusemedien/cron-export';

		if($request->get('salt','string') == $service->getProperty($name, 'salt') && $request->get('salt','string') != ''){
			Util::export();
		}else{
			sly_Util_HTTP::tempRedirect( sly_Core::getSiteStartArticleId() );
		}
		
	}
	
	/**
	 * Callback für SLY_FRONTEND_ROUTER
	 *
	 * Fügt dem übergebenen Router die Routen für diesen Controller hinzu.
	 *
	 * @param  array $params    Eventparameter
	 * @return sly_Router_Base  der übergebene Router
	 */
	public static function addRoutes(array $params) {
		$router = $params['subject']; // sly_Router_Base
		$router->addRoute('/cronexport/:salt', array(
			'slycontroller' => 'cronexport',
			'slyaction' => 'index'
		));
		return $router;
	}

	
}