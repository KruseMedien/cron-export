<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
namespace sly\CronExport;
use sly_Core;
class Util {

	public static function export() {
		$container  = sly_Core::getContainer(); 
		$request = sly_Core::getRequest();
		$service     = $container['sly-service-addon'];
		$name        = 'krusemedien/cron-export';
		
		$includeDump   = true;
		$addUsers      = $service->getProperty($name, 'users');
		$comment       = $service->getProperty($name, 'comment');
		$filename      = 'ce_sly_'.date('Ymd');

		$flash    = $container['sly-flash-message'];
		$service  = $container['sly-importexport-service'];
		$exporter = $container['sly-importexport-exporter'];
		$ceConfig = $container['sly-cronexport-service']->getConfig(null);
		if($ceConfig['deletebackups']){
			self::delete();
		}

		try {
			// setup the exporter

			$exporter
				->includeDump($includeDump)
				->includeUsers($addUsers)
				->setComment($comment)
				->setName($filename)
			;

			// kill all old temp files

			$service->cleanup();

			$exportFile = $exporter->export();

			return basename($exportFile).' exported';
		}
		catch (Exception $e) {
			$flash->addWarning($e->getMessage());
		}
	
	}


	public static function delete() {
		$archives = self::getExpiredBackups();
		if(count($archives) > 0){
			foreach($archives as $archiv) {
				self::deleteBackup($archiv['filename']);
			}
		}
	}

	public static function getExpiredBackups() {
		$container = sly_Core::getContainer();
		$config    = $container['sly-cronexport-service']->getConfig(null);
		$archives = self::getCronBackups();
		$oldArchives = array();
		$currentTime = time();
		$delay = $config['delay'];
		$deletetime = $currentTime - $delay;

		if(count($archives) > $config['backupcount']) {
            $i = 1;
            foreach($archives as $archiv){
                if($i >= (count($archives) - $config['backupcount'])) break;
                if($archiv['date'] < $deletetime) {
                    array_push($oldArchives, $archiv);
                }
                $i++;
            }
        }
		return $oldArchives;
	}

	public static function deleteBackup($filename) {
		$container  = sly_Core::getContainer(); 
		$service  = $container['sly-importexport-service'];
		return $service->deleteArchive($filename);
	}

	public static function getCronBackups() {
		$container  = sly_Core::getContainer(); 
		$service  = $container['sly-importexport-service'];
		$archives = $service->getArchives();

		// sort archives by date
		foreach ($archives as $idx => $archive) {
			$archives[$idx] = $service->getArchiveInfo($archive);
		}

		usort($archives, function($a1, $a2) {
			$date1 = $a1['date'];
			$date2 = $a2['date'];

			return $date1 === $date2 ? 0 : ($date1 > $date2 ? -1 : 1);
		});

		$archives = array_reverse($archives);

		$ceArray = array();
		foreach($archives as $archiv){
			if(substr($archiv['name'],0,2) == 'ce'){
				array_push($ceArray, $archiv);
			}
		}
		return $ceArray;
	}

}